output "simple_log_app_url" {
  value = "http://${aws_eip.swarm_eip.public_ip}:8000"
}
