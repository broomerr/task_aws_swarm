from django.conf.urls import url
from secondapp import views


urlpatterns = [
    url(r'^$', views.HomePageView.as_view()),
    url(r'^logs.html$', views.LogPageView.as_view()),
    url(r'^logs$', views.LogPageView.as_view()),
    # url('index2.html', views.HomePageView.index2()),
]
