resource "aws_instance" "worker1" {
  count         = "${var.swarm_workers}"
  ami           = "${var.image_name}"
  instance_type = "${var.swarm_workers_instance_type}"
  # key_name               = "${var.key_name}"
  key_name = aws_key_pair.default.key_name

  user_data              = "${file("${var.bootstrap_path}")}"
  vpc_security_group_ids = ["${aws_security_group.sgswarm.id}", aws_security_group.main-node-web.id]
  depends_on             = ["aws_instance.master"]
  tags = {
    instance = "worker${count.index + 1}"
    Name     = var.myname
  }
}
