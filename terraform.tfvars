# This file MUST be called terraform.tfvars (or will be  ignored)
region                      = "eu-west-1"
credentials                 = "credentials.json"
project                     = "davydovswarm1"
myname                      = "Stanislav_Davydov"
swarm_masters               = 1
swarm_workers               = 1
swarm_masters_instance_type = "t2.micro"
swarm_workers_instance_type = "t2.micro"
image_name                  = "ami-0862aabda3fb488b5"
image_default_user          = "ec2-user"
ssh_public_key              = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAjRXfWF6v2uYo6szlQTJXnbxgxXwnpdDhYZz6xREnlCGLH60HCe75zA1vv0WtiZdXZfTIt+ZKMawMV3SL+BdLnkxCAL1usNkNvDxDlOk4QXgHzX+VY5mw0XyC6V/eJCi5kYxZnQnFIioqLttvADagqmFBZ6FxOd+wSf0r4Th4a5d7q5aY4e0wDE4vx/GTABnFmNXQLW++KqPFyz+78+CXlXT9DTuvd8HSmIgbE+zt64zsDuPYSpu5xoiV9ULfw9TQjdZ5PJhy9zO5YzYKYu24vwv2kuWlwILO8JBaJ0pGAo/it6EFhcrExNhBOcrUTWkA8rnykQId38ZF2rVzoh+1YQ=="
# ssh_pubkey_path             = "C:/Users/davs.EZWIM/.ssh/id_rsa.pub"
# ssh_privkey_path            = "C:/Users/davs.EZWIM/.ssh/id_rsa"
ssh_pubkey_path  = "/home/vagrant/.ssh/id_rsa.pub"
ssh_privkey_path = "/home/vagrant/.ssh/id_rsa"
