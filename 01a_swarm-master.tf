resource "aws_instance" "master" {
  ami                    = var.image_name
  instance_type          = var.swarm_masters_instance_type
  key_name               = aws_key_pair.default.key_name
  user_data              = "${file("${var.bootstrap_path}")}"
  vpc_security_group_ids = [aws_security_group.sgswarm.id, aws_security_group.main-node-web.id]
  tags = {
    instance = "master"
    Name     = var.myname

  }
}

resource "aws_key_pair" "default" {
  key_name   = "${var.myname}-keypair"
  public_key = "${file("${var.ssh_pubkey_path}")}"
}
