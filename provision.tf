data "template_file" "inventory" {
  template = "${file("inventory.tpl")}"

  depends_on = [
    "aws_instance.master",
    "aws_instance.worker1",
  ]

  vars = {
    masters            = "${join("\n", aws_eip.swarm_eip.*.public_ip)}"
    workers            = "${join("\n", aws_instance.worker1.*.public_ip)}"
    image_default_user = var.image_default_user
    ssh_privkey_path   = var.ssh_privkey_path
  }
}

# not working  on Windows
# resource "null_resource" "cmd" {
#   triggers = {
#     template_rendered = "${data.template_file.inventory.rendered}"
#   }
#
#   provisioner "local-exec" {
#     command = "echo '${data.template_file.inventory.rendered}' > ${path.module}/ansible_inventory"
#   }
#   # provisioner "file" {
#   #   content     = "${data.template_file.inventory.rendered}"
#   #   destination = "${path.module}/ansible_inventory2"
#   # }
#   # provisioner "local_file" {
#   #   content     = "${data.template_file.inventory.rendered}"
#   #   destination = "${path.module}/ansible_inventory2"
#   # }
# }

# module "localfile_file" {
#   source = "github.com/mcoffin/terraform-provider-localfile"
#   name   = "localfile_file"
#
# }
#
# resource "localfile_file" "helloworld" {
#   path    = "helloworld.txt"
#   content = "${data.template_file.inventory.rendered}"
# }

resource "local_file" "inventory" {
  content  = "${data.template_file.inventory.rendered}"
  filename = "${path.module}/ansible_inventory"

  # need to wait until instances become available
  provisioner "local-exec" {
    command = "sleep 60"
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ansible_inventory playbook.yml"
  }

}

# let fail if  above  file  not created
data "local_file" "example" {
  // Ensure this is evaluated only *after* the above provisioner runs
  depends_on = ["local_file.inventory"]

  filename = "${path.module}/ansible_inventory"
}

output "result" {
  value = "${data.local_file.example.content}"
}
