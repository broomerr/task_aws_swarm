[masters]
${masters}

[workers]
${workers}

[all:vars]
ansible_ssh_user=${image_default_user}
ansible_ssh_private_key_file=${ssh_privkey_path}
