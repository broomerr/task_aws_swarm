from django.shortcuts import render
from django.views.generic import TemplateView

from docker import Client
from urllib.parse import urlsplit, parse_qs

# Create your views here.
class HomePageView(TemplateView):
    def get(self, request, **kwargs):
        # client = Client(base_url='unix://var/run/docker.sock')
        client = Client.from_env()
        print(client.nodes())
        for node in client.nodes():
            print(node['Spec']['Role']+ '=' + node['ID'])
        for container in client.containers():
            print(container['Id'])

        return render(request, 'index2.html', {'client': client , 'nodes': client.nodes(), 'containers':client.containers() })

    def index2(self, request):
        # client = Client(base_url='unix://var/run/docker.sock')
        client = Client.from_env()
        return render(request,'index2.html', {'client': client })

class LogPageView(TemplateView):
    def get(self, request, **kwargs):
        # myparams=request[headers]
        # client = Client(base_url='unix://var/run/docker.sock')
        client = Client.from_env()
        #print(request.META)
        params = parse_qs(request.META['QUERY_STRING'])
        #print(params)
        log_result = client.logs(params["id"][0], stream=False)
        #print(log_result)

    #     for node in client.nodes():
    #         print(node['Spec']['Role']+ '=' + node['ID'])
    #     for container in client.containers():
    #         print(container['Id'])
    #
        return render(request, 'logs.html', {'client': client , 'nodes': client.nodes(), 'containers':client.containers() , 'request': request , 'params': params , 'log_result': log_result.decode(encoding='UTF-8').replace('\n', '<br>'), 'log_result_split': log_result.decode(encoding='UTF-8').splitlines() })
    #
    # def index2(self, request):
    #     # client = Client(base_url='unix://var/run/docker.sock')
    #     client = Client.from_env()
    #     return render(request,'index2.html', {'client': client })
