resource "aws_security_group" "sgswarm" {
  name = "${var.myname}-sgswarm"
  tags = {
    Name2 = "${var.project}-sgswarm"
    name  = "davydov"
  }

  # Allow SSH
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  # swarm + remote mgmt
  ingress {
    from_port = 2377
    to_port   = 2377
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  # swarm
  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  # swarm
  ingress {
    from_port = 4789
    to_port   = 4789
    protocol  = "udp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  # swarm
  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "udp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  # swarm ESP (overlay with encryption)
  ingress {
    from_port = -1
    to_port   = -1
    protocol  = "50"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  # Enable ICMP
  ingress {
    from_port = -1
    to_port   = -1
    protocol  = "icmp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
}

resource "aws_security_group" "main-node-web" {
  name = "${var.project}-web-inbound"
  # vpc_id = module.vpc.vpc_id
  tags = {
    Name2 = "${var.project}-sgweb"
    name  = "davydov"
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  ingress {
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  egress {
    from_port = 1
    to_port   = 60000
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

}



# This is your elastic IP. When you have to redeploy your main node your IP will stay the same because of this :)
resource "aws_eip" "swarm_eip" {
  instance = aws_instance.master.id
  vpc      = true

  tags = {
    Name2 = "${var.project}-eip"
    name  = "davydov"
  }
}
