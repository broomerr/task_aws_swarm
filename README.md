# AWS: Docker SWARM with terraform and ansible

## Quickstart
### Preparation tasks
Here assumed that  on host in file "credentials" (without extention)  configured AWS profile "opsworks" (see provider.tf) like this:
```
[opsworks]
aws_access_key_id = XXXXXXXXXXXXXXX
aws_secret_access_key = YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
```
If  you want to use it as default profile you need write [default] in file "credentials" and  in file provider.tf change "opsworks" to "default".

### Commands
* terraform apply

### Results
Above command will create AWS environment with two hosts (see swarm_masters/swarm_workers in terraform.tfvars).

Then with command below (as part of provision.tf) created  Docker swarm and created service in it.
* ansible-playbook -i ansible_inventory playbook.yml

## Prerequisite
It is assumes that was created docker container broomerr/simple_docker_logview and publiched  on Docker Hub.
Name of  container hardcoded in playbook.yml

### Creation of  container
Mentioned container was created with such commands:
* docker build -t simple_docker_logview .
* docker tag simple_docker_logview broomerr/simple_docker_logview:latest
* docker push broomerr/simple_docker_logview:latest


## Used
* https://github.com/ruanbekker/ansible-docker-swarm
