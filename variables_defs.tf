variable "region" {
}

#variable "aws_key" {}
#variable "aws_secret" {}

variable "credentials" {
}

variable "project" {
}
variable "myname" {}
variable "swarm_masters" {}
variable "swarm_workers" {}
variable "swarm_masters_instance_type" {}
variable "swarm_workers_instance_type" {}
variable "image_name" {}
variable "ssh_public_key" {}
variable "ssh_pubkey_path" {}
variable "ssh_privkey_path" {}
variable "bootstrap_path" {
  description = "Script to install Docker Engine"
  default     = "install-docker.sh"
}
variable "image_default_user" {}
